# Converge Frontend Test

## Coding challenge

If you’re reading this, that means that Converge is really interested in your engineering abilities. Congratulations!

This is intended to be a small coding exercise meant to gauge your programming style. Now’s your chance to show off your design skills (simple but extensible), your testing habits (hey hey unit-tests), and your keen attention to detail.

## Submission

Fork this repo and commit your changes like you would if you were coding on the job. Once your code is ready, just send it to us. It's that easy! Please include

* A `README.md` file with instructions on how to run your program

## The problem

[The COVID Tracking Project](https://covidtracking.com/data/api/version-2) at The Atlantic has an API that serves COVID data via REST. As of March 7, 2021 they do not collect new data, however the API still serves the data they had collected.

Your job is to create a form that determines which REST API endpoint to hit, and then render the data in a meaningful way. The form should be able to specify a state / territory if requested, though not required. When a state is specifed, the form should enable filtering of the data based on cases, tests, and outcomes. Feel free to add other filters as well.

You'll notice that we are being very loose on the guidelines here. That is because we want you to get creative on how you ingest and present data while also adhereing to production level coding standards.

Questions?

Feel free to email.

Good luck!
Your friends at Converge

<img src="https://media-exp1.licdn.com/dms/image/C4D1BAQFQRJMSEXR-PQ/company-background_10000/0/1648847020596?e=1662141600&v=beta&t=xqqqw2dX3kQR5quGPegWBY3MFckD7CT9_233ZQiC3ao">
